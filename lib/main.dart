import 'package:flutter/material.dart';
import 'keep_page.dart';

void main()=>runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Keep page status',
      theme: ThemeData(primarySwatch: Colors.lightBlue),
      home: KeepPageAlive(),
    );
  }
}

class KeepPageAlive extends StatefulWidget {
  @override
  _KeepPageAliveState createState() => _KeepPageAliveState();
}

class _KeepPageAliveState extends State<KeepPageAlive> with SingleTickerProviderStateMixin {
  TabController _controller;

  @override
  void initState() { 
    super.initState();
    _controller=TabController(length: 3, vsync: this);  //from SingleTickerProviderStateMixin means verticality 
  }

  @override
  void dispose() { 
    _controller.dispose();
    super.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Keep Page Alive'),
        bottom: TabBar(
          controller: _controller,
          tabs: <Widget>[
            Tab(icon: Icon(Icons.directions_car)),
            Tab(icon: Icon(Icons.directions_transit)),
            Tab(icon: Icon(Icons.directions_bike)),
          ],
        )),
        body: TabBarView(
          controller: _controller,
          children: <Widget>[
            // Text('Car ...'),
            // Text('Metro ...'),
            // Text('Bike ...'),

            MyPage(),
            MyPage(),
            MyPage()
          ],
        ),
    );
  }
}